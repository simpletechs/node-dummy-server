var server = require('../../server');

module.exports = function(port) {
    before(function() {
        server.listen(port);
    });

    after(function() {
        server.close();
    });
};