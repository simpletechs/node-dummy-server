var expect = require('chai').expect,
    server = require('./lib/server'),
    superagent = require('superagent'),
    port = 8000;

server(port);

describe('homepage', function(){
    it('should respond to GET',function(done){
        superagent
            .get('http://localhost:'+port)
            .end(function(err, res){
                expect(res.status).to.equal(200);
                done();
            });
    });
});