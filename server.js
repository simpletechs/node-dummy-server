var http = require('http'),
    randomID = Math.random();

var server = http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.write('wazzup, id ('+randomID+'). / ' + process.version + ' '+JSON.stringify(process.env, null, 4));
  res.end();
});

module.exports.listen = function(port) {
    server.listen(port);
    console.log('The http server has started at: :' + port);
}
module.exports.close = function(callback) {
    server.close(callback);
}

if (require.main === module) {
    module.exports.listen(80);
}